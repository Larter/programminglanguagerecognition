from DataExtractor import  DataExtractor
from DictionaryCombiner import  DictionaryCombiner
from FileParser import FileParser

class DataReader:
    def __init__(self):
        self.__learn_data = []
        self.__learn_values = []
        self.__dataExtractor = DataExtractor([])
        self.__languageDict = dict()

    def read_learn_data(self, files):

        parser = FileParser()
        combiner = DictionaryCombiner()
        combined = []
        technology_dicts = {}
        for technology, fileList in files.items():
            dictionaries = []
            for file in fileList:
                dictionaries.append(parser.parse(file))
            combined += combiner.combine(dictionaries, 0.2)
            technology_dicts[technology] = dictionaries

        combined = list(set(combined))
        combined.sort()
        self.__dataExtractor = DataExtractor(combined)

        self.__languageDict = {}
        index = 0
        for key in technology_dicts:
            self.__languageDict[key] = [0] * (len(technology_dicts))
            self.__languageDict[key][index] = 1
            index += 1

        for technology, examples in technology_dicts.items():
            for example in examples:
                self.__learn_data.append(self.__dataExtractor.extract(example))
                self.__learn_values.append(self.__languageDict[technology])

        #print(combined)
        #print(len(combined))
        print(self.__languageDict)
        #print(self.__learn_data)
        #print(self.__learn_values)

    def get_extractor(self):
        return self.__dataExtractor

    def get_language_dict(self):
        return self.__languageDict

    def get_learn_data(self):
        return self.__learn_data, self.__learn_values