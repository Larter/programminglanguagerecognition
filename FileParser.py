from collections import defaultdict

class FileParser:
    def parse(self, filename):
        dictionary = defaultdict(float)
        try:
            with open(filename, encoding="utf8") as f:
                wordCount = 0
                currentWord = ''
                while True:
                    c = f.read(1)
                    if not c:
                        if currentWord is not '':
                            dictionary[currentWord] += 1
                            currentWord=''
                            wordCount += 1
                        break

                    if c.isalpha() or c.isnumeric():
                        currentWord+=c
                        continue
                    else:
                        if currentWord is not '':
                            dictionary[currentWord] += 1
                            currentWord = ''
                            wordCount += 1
                        if c != ' ' and c != '\n':
                            dictionary[c]+=1
                            wordCount += 1

                for x in dictionary:
                    dictionary[x] /= wordCount

        except Exception as exception:
            print(filename)

        return dictionary
