from DictionaryCombiner import DictionaryCombiner

def test_combine_two_same_shall_give_list_of_the_same():
    combiner = DictionaryCombiner()
    percentage_to_use = 0
    result = combiner.combine([{'a': 0.3, 'b': 0.1}, {'a': 0.2, 'b': 0.4}], percentage_to_use)
    assert result == ['a', 'b']


def test_combine_two_other_with_0_percentage_shall_add_all_to_list():
    combiner = DictionaryCombiner()
    percentage_to_use = 0
    result = combiner.combine([{'a': 0.3}, {'b': 0.4}], percentage_to_use)
    assert result == ['a', 'b']


def test_combine_two_other_with_1_percentage_shall_not_add_all_to_list():
    combiner = DictionaryCombiner()
    percentage_to_use = 1
    result = combiner.combine([{'a': 0.3}, {'b': 0.4}], percentage_to_use)
    assert result == []


def test_many_dictionaries_mixed_percentage():
    combiner = DictionaryCombiner()
    percentage_to_use = 0.25
    dictionaries = [{'a': 2, 'b': 4},
                    {'a': 2, 'c': 4},
                    {'c': 2, 'd': 4},
                    {'w': 2, 'a': 4}]
    result = combiner.combine(dictionaries, percentage_to_use)
    assert result == ['a', 'b', 'c', 'd', 'w']
    percentage_to_use = 0.5
    result = combiner.combine(dictionaries, percentage_to_use)
    assert result == ['a', 'c']

    percentage_to_use = 0.75
    result = combiner.combine(dictionaries, percentage_to_use)
    assert result == ['a']

    percentage_to_use = 1
    result = combiner.combine(dictionaries, percentage_to_use)
    assert result == []

