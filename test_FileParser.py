from FileParser import  FileParser

def test_one_sign_file():
    parser = FileParser()
    result = parser.parse("ReadTests/OneSignFile.txt")
    assert result == {'a': 1};


def test_two_different_sign_file():
    parser = FileParser()
    result = parser.parse("ReadTests/TwoDifferentSignFile.txt")
    assert result == {'a': 0.5, 'b': 0.5};

def test_repeating_word():
    parser = FileParser()
    result = parser.parse("ReadTests/RepeatingWords.txt")
    assert result == {'a': 0.5, 'b': 0.25, 'c':0.25};

def test_full_word():
    parser = FileParser()
    result = parser.parse("ReadTests/FullWordFile.txt")
    assert result == {'word1':1};

def test_signs_with_words():
    parser = FileParser()
    result = parser.parse("ReadTests/SignsWithWords.txt")
    assert result == {'(' : 0.25, ')': 0.25, ';':0.25, 'it':0.125, 'some':0.125}
