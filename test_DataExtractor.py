from DataExtractor import DataExtractor

def test_init_and_get_keys():
    keys_to_use = ['key']
    extractor = DataExtractor(keys_to_use)
    assert keys_to_use == extractor.get_keys()


def test_extract_single_key():
    keys_to_use = ['a']
    extractor = DataExtractor(keys_to_use)
    data = {'a': 0.3}
    result = extractor.extract(data)
    assert result == [0.3]


def test_extract_two_keys():
    keys_to_use = ['a', 'b', 'c']
    extractor = DataExtractor(keys_to_use)
    data = {'a': 0.3, 'b': 0.4}
    result = extractor.extract(data)
    assert result == [0.3, 0.4, 0]


def test_extract_two_keys_in_data_one_in_valuable_keys():
    keys_to_use = ['a', 'b']
    extractor = DataExtractor(keys_to_use)
    data = {'a': 0.3, 'b': 0.4, 'd': 0.2}
    result = extractor.extract(data)
    assert result == [0.3, 0.4]


def test_mixed_number_of_keys_and_values():
    keys_to_use = ['a', 'b', 'c']
    extractor = DataExtractor(keys_to_use)
    data = {'a': 0.3, 'b': 0.4, 'd': 0.3}
    result = extractor.extract(data)
    assert result == [0.3, 0.4, 0]
