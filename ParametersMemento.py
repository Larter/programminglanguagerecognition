import json

class ParametersMemento:
    def __init__(self, filename):
        self.__filename = filename

    def save(self, language_dict, extractor_keys):
        data = { "language_dict": language_dict, "keys" : extractor_keys}
        json_data = json.dumps(data)
        print(json_data)
        with open(self.__filename, "w") as text_file:
            print(json_data, file=text_file)

    def load(self):
        with open(self.__filename) as data_file:
            data = json.load(data_file)
            return data["language_dict"], data['keys']






