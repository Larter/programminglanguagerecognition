from ParametersMemento import ParametersMemento


def test_single_file_save():
    memento = ParametersMemento("test_file.json")
    dictionary = {"w": [0, 1], 'z': [1, 0]}
    keys = ['a', 'b', 'c']
    memento.save(dictionary, keys)
    res_dict, res_keys = memento.load()
