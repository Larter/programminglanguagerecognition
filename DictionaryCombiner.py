from collections import defaultdict
from collections import Counter


class DictionaryCombiner():
    def combine(self, dictionaries, percentage_to_accept):
        allWords = []
        for d in dictionaries:
            allWords += d.keys()

        wordsCounter = Counter(allWords)
        result = []
        for word in wordsCounter:
            if float(wordsCounter[word])/float(len(dictionaries)) >= percentage_to_accept:
                result.append(word)
        result.sort()
        return result
