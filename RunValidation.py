from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys

# Import data
import itertools
from tensorflow.examples.tutorials.mnist import input_data


import tensorflow as tf
import os

from ParametersMemento import ParametersMemento
from FileParser import FileParser
from DataExtractor import DataExtractor

input_filename = None

def listdir_full_path(d):
    return [os.path.join(d, f) for f in os.listdir(d)]

def prepare_language_list(language_dictionary):

    language_list =[]
    for language in language_dictionary:
        language_list.append((language, language_dictionary[language].index(1)))

    return language_list


def main(_):
    memento = ParametersMemento("models/parameters.json")
    try:
        language_dictionary, keys = memento.load()
    except:
        print("No parameters, please train the network first ")
        return

    x_size = len(keys)
    y_size = len(language_dictionary)
    x = tf.placeholder(tf.float32, [None, x_size])
    W = tf.Variable(tf.zeros([x_size, y_size]))
    b = tf.Variable(tf.zeros([y_size]))
    x1 = tf.placeholder(tf.float32, [None, x_size])
    y1 = tf.matmul(x1, W) + b
    y = tf.matmul(x, W) + b
    y_ = tf.placeholder(tf.float32, [None, y_size])
    saver = tf.train.Saver()

    extractor = DataExtractor(keys)
    parser = FileParser()

    sess = tf.InteractiveSession()

    tf.global_variables_initializer().run()

    saver.restore(sess, "models/model.ckpt")
    print("Model restored.")
    print(language_dictionary)

    
    x_python_validation = []
    y_python_validation = []
    for file in listdir_full_path("ValidationData/Python"):
        x_python_validation.append(extractor.extract(parser.parse(file)))
        y_python_validation.append(language_dictionary["python"])

    x_cpp_validation = []
    y_cpp_validation = []
    for file in listdir_full_path("ValidationData/Cpp"):
        x_cpp_validation.append(extractor.extract(parser.parse(file)))
        y_cpp_validation.append(language_dictionary["c++"])

    x_java_validation = []
    y_java_validation = []
    for file in listdir_full_path("ValidationData/Java"):
        x_java_validation.append(extractor.extract(parser.parse(file)))
        y_java_validation.append(language_dictionary["java"])

    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    print("Python accuracy: ")
    print(sess.run(accuracy, feed_dict={x: x_python_validation,
                                        y_: y_python_validation}))

    print("C++ accuracy: ")
    print(sess.run(accuracy, feed_dict={x: x_cpp_validation,
                                        y_: y_cpp_validation}))
    print("Java accuracy: ")
    print(sess.run(accuracy, feed_dict={x: x_java_validation,
                                        y_: y_java_validation}))


    print("full accuracy ", sess.run(accuracy, feed_dict={x: x_python_validation + x_java_validation + x_cpp_validation,
                                                      y_: y_python_validation + y_java_validation + y_cpp_validation}))

if __name__ == '__main__':
    tf.app.run(main=main)
