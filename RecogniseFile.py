from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys

# Import data
import itertools
from tensorflow.examples.tutorials.mnist import input_data


import tensorflow as tf

from ParametersMemento import ParametersMemento
from FileParser import FileParser
from DataExtractor import DataExtractor

input_filename = None


def prepare_language_list(language_dictionary):

    language_list =[]
    for language in language_dictionary:
        language_list.append((language, language_dictionary[language].index(1)))

    return language_list


def main(_):
    memento = ParametersMemento("models/parameters.json")
    try:
        language_dictionary, keys = memento.load()
    except:
        print("No parameters, please train the network first ")
        return

    x_size = len(keys)
    y_size = len(language_dictionary)
    x = tf.placeholder(tf.float32, [None, x_size])
    W = tf.Variable(tf.zeros([x_size, y_size]))
    b = tf.Variable(tf.zeros([y_size]))
    x1 = tf.placeholder(tf.float32, [None, x_size])
    y1 = tf.matmul(x1, W) + b
    y = tf.matmul(x, W) + b
    y_ = tf.placeholder(tf.float32, [None, y_size])
    saver = tf.train.Saver()

    extractor = DataExtractor(keys)
    parser = FileParser()

    sess = tf.InteractiveSession()

    tf.global_variables_initializer().run()

    saver.restore(sess, "models/model.ckpt")
    print("Model restored.")
    print(language_dictionary)
    language_list = prepare_language_list(language_dictionary)
    prediction = tf.argmax(y, 1)
    test_feed_dict = {x: [extractor.extract(parser.parse(input_filename))]}
    best = sess.run(prediction, test_feed_dict)
    print (best)
    for predicted_language in filter(lambda language_tuple: best[0] == language_tuple[1], language_list):
        print("File " + input_filename + " is " + predicted_language[0])




if __name__ == '__main__':
    input_filename = sys.argv[1]
    tf.app.run(main=main)
