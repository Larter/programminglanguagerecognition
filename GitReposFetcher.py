import glob
import json
import os
import shutil
from random import randint
from subprocess import call

class GitReposFetcher:

    def fetch(self):
        necessary_directories = ["ExampleData", "FetchedRepos", "TestData", "ValidationData"]
        self.initialize_catalogs(necessary_directories)
        parsed_json = json.load(open("GitReposToFetch.json"))
        for language_index in parsed_json:
            language = parsed_json[language_index]
            for repo in language["repos"]:
                    path = "FetchedRepos/"+repo.split("/")[-1].split(".")[0]
                    if not os.path.exists(path):
                        os.mkdir(path)
                        call(["git", "clone", repo, path])
            self.initialize_catalogs(["ExampleData/" + language["directoryName"],
                                      "TestData/" + language["directoryName"],
                                      "ValidationData/" + language["directoryName"]])
            for extension in language["extensions"]:
                for filename in glob.iglob('FetchedRepos/**/'+extension, recursive=True):
                    if os.stat(filename).st_size > 5 * 1024:
                        directory = self.__random_training_or_test_directory()
                        shutil.copy(filename, directory + language["directoryName"])

    def initialize_catalogs(self, catalogs):
        for catalog in catalogs:
            if not os.path.exists(catalog):
                os.mkdir(catalog)

    def __random_training_or_test_directory(self):
        random_number = randint(0, 9)
        return "TestData" if random_number == 0 else "ValidationData" if random_number == 1 else "ExampleData"