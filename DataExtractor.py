class DataExtractor:
    def __init__(self, valueable_keys):
        self.__valueable_keys = valueable_keys

    def get_keys(self):
        return self.__valueable_keys

    def extract(self, input):
        result = []
        for key in self.__valueable_keys:
            if key in input:
                result.append(input[key])
            else:
                result.append(0)
        return result
