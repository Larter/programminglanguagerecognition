from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from GitReposFetcher import GitReposFetcher
from ParametersMemento import  ParametersMemento
import random
from ParametersMemento import ParametersMemento

import argparse
import os
import sys

# Import data
from tensorflow.examples.tutorials.mnist import input_data

import tensorflow as tf

from  FileParser import FileParser
from  DataReader import DataReader



def listdir_full_path(d):
    return [os.path.join(d, f) for f in os.listdir(d)]


def main(_):
    reader = DataReader()
    GitReposFetcher().fetch()

    files = {
        "c++": listdir_full_path("ExampleData/Cpp/"),
        "python": listdir_full_path("ExampleData/Python/"),
        "java": listdir_full_path("ExampleData/Java")
    }
    reader.read_learn_data(files)
    batch_xs, batch_ys = reader.get_learn_data()

    x_size = len(reader.get_extractor().get_keys())
    y_size = len(reader.get_language_dict())

    x = tf.placeholder(tf.float32, [None, x_size])
    W = tf.Variable(tf.zeros([x_size, y_size]))
    b = tf.Variable(tf.zeros([y_size]))
    y = tf.matmul(x, W) + b
    # Define loss and optimizer
    y_ = tf.placeholder(tf.float32, [None, y_size])

    saver = tf.train.Saver()
    parser = FileParser()

    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(y, y_))
    train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

    sess = tf.InteractiveSession()
    tf.global_variables_initializer().run()

    x_python_test = []
    y_python_test = []
    for file in listdir_full_path("TestData/Python"):
        x_python_test.append(reader.get_extractor().extract(parser.parse(file)))
        y_python_test.append(reader.get_language_dict()["python"])

    x_cpp_test = []
    y_cpp_test = []
    for file in listdir_full_path("TestData/Cpp"):
        x_cpp_test.append(reader.get_extractor().extract(parser.parse(file)))
        y_cpp_test.append(reader.get_language_dict()["c++"])

    x_java_test = []
    y_java_test = []
    for file in listdir_full_path("TestData/Java"):
        x_java_test.append(reader.get_extractor().extract(parser.parse(file)))
        y_java_test.append(reader.get_language_dict()["java"])

    # Test trained model
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    for i in range(5000):
        xses = []
        yses = []
        chunk_size = 10
        for _ in range(20):
            randomStart = random.randint(0, len(batch_xs) - chunk_size)
            xses += batch_xs[randomStart:randomStart+chunk_size]
            yses += batch_ys[randomStart:randomStart+chunk_size]
        sess.run(train_step, feed_dict={x: xses, y_: yses})
        if i % 100 == 0:
            print("iteration ", i)
            print("test accuracy ", sess.run(accuracy, feed_dict={x: x_python_test +x_java_test + x_cpp_test,
                                                y_: y_python_test + y_java_test + y_cpp_test}))
            print("learn accuracy ", sess.run(accuracy, feed_dict={x: batch_xs, y_: batch_ys}))



    print("Python accuracy: ")
    print(sess.run(accuracy, feed_dict={x: x_python_test,
                                        y_: y_python_test}))

    print("C++ accuracy: ")
    print(sess.run(accuracy, feed_dict={x: x_cpp_test,
                                        y_: y_cpp_test}))
    print("Java accuracy: ")
    print(sess.run(accuracy, feed_dict={x: x_java_test,
                                        y_: y_java_test}))

    memento = ParametersMemento("models/parameters.json")
    memento.save(reader.get_language_dict(), reader.get_extractor().get_keys())
    saver.save(sess, "models/model.ckpt")


if __name__ == '__main__':
    tf.app.run(main=main)
