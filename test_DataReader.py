from  DataReader import DataReader

def test_data_reader_check():
    reader = DataReader()

    files = {
        "c++":
            [
                "ExampleData/Cpp/1.cpp",
                "ExampleData/Cpp/2.cpp",
                "ExampleData/Cpp/3.cpp",
                "ExampleData/Cpp/4.cpp",
                "ExampleData/Cpp/5.cpp",
            ],
        "python":
            [
                "ExampleData/Python/1.py",
                "ExampleData/Python/2.py",
                "ExampleData/Python/3.py",
                "ExampleData/Python/4.py",
                "ExampleData/Python/5.py",

            ]
    }
    reader.read_learn_data(files)
